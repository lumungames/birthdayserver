***** UPDATED FOR ALPHA 21 ***** 


!!!!!  REQUIRES A NEW GAME FOR BEST RESULTS  !!!!!


After extracting the downloaded 7z file contents just place the entire folder
"FPS Cleaner Roads and Biomes" into your Mods folder and you should be ready to go. 
Installs just like almost every mod. 

THIS WILL REQUIRE A NEW GAME FOR FULL EFFECT.

You will experience a partial effect for areas that you have NOT visited on an 
existing save.

This mod removes clutter and trash from the Wasteland and Burnt Forest and
it cleans all the "decals" off the roads. It removes all the glowing ember piles
from the Burnt Forest and all the landmines from the Wasteland along with the
concrete blocks, brick piles and scrap metal piles. 

If there is a POI in the Wasteland that has landmines, they will still have them.
The random ones are all gone!

There are still some items like loot containers and vehicles in the roads. This 
was left INTENTIONALLY because a lot of these areas are spawn points. Junk like 
tires and dead animals are gone... along with the "phoney" decals on the roads.
Makes for a much "prettier" look to the game.


I hope you enjoy the new mod much as I have and please let me know if you have
any questions or need help!

I would like to give credit to Zchott for ideas I took from the Reduced Biome Grass
mod and also to KorgX3 for the No Road Decals Mod. Thanks very much!




 



