
 Unnecessarily Beautiful But Immersive - 4.6.5.0
 A21.1 File Size Reduction & Campfire Functionality Removal
 Shoutouts 4.6.5.0


 ================================
 
 
 This file is meant solely to show respect to all the great and amazing modders out there. Some of you specialize in Unity, Blender, Modeling and Textures, while others specialize in XML, XPATH, CSHARP and beyond. I truly appreciate everything you guys have done to help me get to where I am today!
 
 The order of shoutouts is mostly by the day/time I found in Discord. I believe my higest thanks go to Hernan, Xyth, Guppycur, Budbyah, Haidr'Gna and Dayuppy. However, all of you who have taken time out of your day to help me deserve a shoutout!
 
 
 ================================
 
 
 PRE ALPHA 21 ASSISTANCE
 ==============
 
 Xyth: Without Xyth, a lot of this would not have happened. From his tutorials, help early on with moving from the "doorSecure" class to "switch" classes, animations, looping, dummy layers, Localization suggestions, etc. Between occasional direct messages, chime ins on channels and so on. He helped me truly understand a large part of what I've done so far. I was able to also correct errors associated with "MultiBlockDim," the -1 yellow line animator error which is associated with the wrong naming conventions (i.e you have to use SwitchOffStatic and SwitchOnStatic - not OffStatic and OnStatic) and disappearing icons via conversation with Xyth.
 
 Xyth, Guppycur & Bdubyah: Thank you for guiding me to go to Unity early on when I tried to lazily adjust the blocks.xml to utilize the "switch" class when nothing was designed to work like a switch. Almost four weeks later and I feel much more knowledgeable and much more confident in this mod with this release.
 
 Xyth & Ragsy: Thank you for helping me insure the TVs were properly bright in-game by making sure to change the shader to "unlit/texture." Thank you Ragsy for also helping me to understand how to work with objects, shaders, textures etc. in Unity. This very note actually helped me do a last minute edit on the laptop - who'd of thought!
 
 Xyth, Guppycur & Mumpfy: Thank you for helping me put the final touches on the mounted TVs so they are correctly mounted to the walls as they should be. Talk about immersion!
 
 Xyth & Guppycur: Thank you for attempting to assist me in creating animations that would allow for two or more screens to work on a TV (flipping from the off state to channel, to channel, to channel, to off state and repeat). I ultimately nixed this idea, as I could not get it to work properly, but I believe my findings may help Xyth crack the code so to speak one day. It is worthy to note, that we found the -1 animator issue when trying to do multiple screens. This was resolved by adding a second layer (making it the primary layer with weight set to 1 and override set) and making layer 0 a dummy layer with empty states and transitions going from "SwitchOffStatic" > "On" > "SwitchOnStatic" > "Off" > "SwitchOffStatic."
 
 Xyth, Mumpfy, Snufkin & TSBX: Thank you for helping me change wording for switches from "Switch light off" / "Switch light on" to "turn off" / "turn on." While this was accomplished through organic conversation and light pushes to look at the vanilla Localization, I wouldn't have known to do that without you guys.
 
 Guppy: Thank you for helping me understand the in and outs of using Unity. Alongside Xyth, I would not have been able to do everything without first having learned to rebuild the first lamp with your assistance. You also helped me with textures and taught me how to use the application Materialize! Finally, without organic conversation with you, I wouldn't have realized that making a Prefab out of an object while it's invisible in Unity and then bundling it, would cause issues in-game. Thank you for also helping me understand how to design doors.
 
 Guppycur, Zorro, Ragsy & Khaine: Thank you guys for helping me rebuild my first lamp from Hernan's Unnecessary But Beautiful mod. This was the only lamp I made new textures for, as I eventually caught on to how to truly rebuild the remaining objects based on what was already designed, but without your guys help, none of this would have been possible.
 
 Guppycur, Bdubyah & saminal: Thank you guys early on for helping me understand why one of my lamps weren't showing in-game and causing a red line error. Also through organic conversation, I was able to to determine that to make sure the icon in the inventory and in the crafting area is showing, the name via the icon in "ItemIconAtlas" and line of code "CustomIcon" in blocks.xml has to be the same.
 
 Zorro & Khaine: Thank you guys for helping me figure out which version of Unity to install to even begin working with Hernan's assets in the first place. I was determined to install all of them, but I'm glad you guys narrowed it down to just 2-4. We figured it out and four weeks later, I have a fully released, massive modlet!
 
 Bdubyah: Thank you for helping me learn how to work around some of the bits of unity in terms of shaders, editing, creating, terminology, etc. Thank you for also helping me add the sound effect to the Sound Bar and helping me make sure it worked in-game and thank you for helping me create my sounds.xml and for helping me learn and understand how to rename the line in sounds.xml "ZTTechRebuilds.unity3d?ZTSoundBarSilentTrigger" to make sure it linked the silent trigger sound. While there is still a lot of work to do on this portion of the mod, your support was appreciated.
 
 Mumpfy: Thank you for helping me understand voxel space in Unity in relation to colliders, alignment etc.
 
 Artjom & Snufkin: Thank you for also supporting me while I was learning how to create my sounds.xml file. Snufkin, I believe your suggestion to make the sounds.xml utilize "AudioSource_Explosion" should make it work more effectively.
 
 saminal: Thank you for helping me understand how containers and doors work in relation to locks. Furthermore, thank you for helping me understand conventions surrounding readme's, correcting my modInfo.xml early on and explaining nodes in xml's.
 
 TSBX: As one of the admins of the Discord, thank you for chiming in and encouraging me to take the next steps when I've asked questions and begun to take initiative. Specifically, without your "Looks good to me, If it works, it works," I wouldn't have moved forward with my need to edit over 5,000 lines of code for consistency, professional appearance, cleanliness and my personal OCD needs in the blocks.xml alone.
 
 sphereii: Thank you for making me utilize notepad++ like I should, by saving my files as .xml while working in them, even for random lines of code in a "backup" file. While I never got the XML Tools plugin, your advice helped me immensely!
 
 wrathmaniac: Thank you for helping me understand Localization's to their full extent. Before my conversations with you, I didn't 100% understand what I was doing, but you helped me learn the in and outs of this txt file. I appreciate your help! Thank you for also helping me use your 7 Days to Die Mod Editor. While I haven't used it for most of my project, it did help me understand the in and outs of the blocks.xml early on.
 
 InnocuousChaos: Thank you for helping me create my progression.xml. Without you, I would not have been able to get the code together or get the lights unlockable the way I was wanting. Thank you for also helping me through ideas for my sound effect for the Sound Bar. Hopefully I scare the crap out of people!
 
 chiko: Thank you for your thorough explanation of offset language and terminology - "Find a table that's less than 1 block high, like a coffee table.  Try putting a regular mug on top of it, and see that it floats.  Now, try putting an offset version of the mug on top of it.  The offset version doesn't float, so it looks nice." While I removed offset versions of lights and electronics etc. from this mod ultimately, it's nice to understand this for the future.
 
 Haidr'Gna: Thank you for helping me understand how to rename my bundles in the line in blocks.xml "Resources/ZTTVRebuilds.unity3d?ZTSmallTVRebuildPrefab" after bundling multiple assets together to make for a cleaner mod.
 
 RazorEye: Thank you for your suggestion to add bedrolls as a feature to the mod. I went ahead and through the feature onto the garden bed for a little bit of fun, but thanks to you and your suggestion, this mod is now additionally more immersive than ever!
 
 BeardMan: With the help of modder Beardman, lights can now be unlocked before hitting the appropriate perks if you are lucky enough to find the "basics of electricity schematic" either through the trader or out in the world on your travels. Beardman designed the xpath which allowed for this schematic to become unlockable. With his help, I also realized that by putting various lights under master blocks it created a loophole, which allowed for select lights to still be crafted despite not having the correct perks assigned. I resolved this by removing lights from different recipe blocks and only having them under one recipe block ("HN Lights" or "Lights").
 
 K1NGER1988: Thank you for providing the xml code for blocks.xml. To enable appropriate storage for a variety of containers, he provided me with the following: 
 change <property name="lootList" value="6" /> to <property name="LootList" value="poiStorageBox" />
 
 Ozmods: Thank you for providing me even more context for container storage information. Specifically, it was very helpful to know the differences between "cupboard," "playerstorage" and "storageCrate." I'll provide your specific notes in the Notes file.
 
 
 ================================
 
 
 ALPHA 21 ASSISTANCE
 ==============
 
 magejosh: I want to give a big thank you to Mage Josh for encouraging me to update UBBI to A21 and and for getting me in the right direction for what changes needed to be made. You also helped me figure out why the TV block was throwing an error due to a naming difference on the A21 master block for TVs.
 
 ocbMaurice & magejosh: I want to thank Josh and Maurice for helping me get the ModInfo updated to A21's formatting.
 
 magejosh & K1NGER1988: Thank you guys for letting me know that I should be fixing naming conventions from "name name123" to either "nameName123" or "Name_Name123." This took hours to fix between the Blocks XML, Localization and others, but it was well worth it!
 
 bdubyah, wolfbain5 & magejosh: Thank you guys for collectively helping me figure out how to fix the progression XML and to learn how the new progression system works so the lights could still be unlockable and for me understand how to unlock them in A21!
 
 zilox: Thank you for identifying that the reason why so many items looked back from the direct port of A20 to A21 was because I needed to use the current version of Unity, 2021.3.19f1.
 
 Haidr'Gna, wolfbain5 & magejosh: After much discussion amongst the group in Discord, you all lead me to finding an issue with a naming convention / incorrect name of a block that wasn't matching between the main block name vs the VariantHelper block, Localization, CustomIcon and ItemIconAtlas. Since the variant helper had "HN_UmbrellaRed" and the block had "HN UmbrellaRed," it was throwing error "EXC Object reference not set to an instance of an object." Gna gave me the suggestion to test the blocks one at a time, which ultimately lead to me finding the exact problem on this.
 
 Blue name: You provided me with soem glass textures. I believe I applied it to at least one item? Either way, I appreciate you for getting this sent my way!
 
 Dayuppy: I want to thank you for your light weight script "Dayuppy Tools," which allowed me to build Unity3D AssetBundle's From Selection using the new LZ4 Chunk-Compressed bundle method. This was so easy to use and was extremely helpful in getting all items from UBBI fully compatible with the A21 verison of 7D2D. Thank you for also helping me figure out how to fix my doors due to the new "Tags" properties in the blocks XML. I also appreciate you getting me the latest "TagManager" asset for Unity. Thank you for identifying that an error was occuring due to a bundle naming issue in the sounds XML.
 
 Guppycur: Thank you for letting me know that blocks and items preload and do not require the items XML preload hack. Further, you explained that entity textures and models load when the entity spawns, and it does not stay in memory very long. So the initial spawn will cause a stutter or lag on your computer. To get by this, modders preload the mesh on an item or block, so that way it's loaded when the game starts and stays loaded. Ultimately it uses more memory, but it removes the spawn lag. This only applies for entities / vehicles. It was for this explanation that I decided to remove the items XML altogether.
 
 Xyth: Thank you for helping me learn how to avoid Z fighting and actually fixing it.
 https://www.unity3dtips.com/unity-z-fighting-solutions/
 
 Dayuppy, Haidr'Gna: I owe a huge thank you to you guys for teaching me about how utilize a grandmaster block, master blocks and then using variant blocks and individual blocks respectively to extend of the masters as needed. Thank you both for also teaching me how to use SortOrder1 and SortOrder 2 properly!
 
 Dayuppy, Sphereii, RevanantWit, Alloc, ocbMaurice and bdubyah: Thank you all for spurring conversation in the xpath channel, as I was having a really hard time adjusting recipes for the various kitchen appliances, while ensuring the campfires were unchanged. I finally got it all figured out, but not without many of your guys help.
 
 
 ================================
 
 
 ALPHA 21.1 ASSISTANCE
 ==============
 
 Beardman: Thank you for pointing out the bug with campfires. This prompted the latest update 4.6.5.
 
 Sphereii: Thank you for letting me know how to call models, meshes, sounds etc from different mods to make the cooking add-on a smooth transition.
 
 
 ================================