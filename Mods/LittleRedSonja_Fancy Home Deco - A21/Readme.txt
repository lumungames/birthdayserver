-- Fancy Home Deco --7 days to die mod - by LittleRedSonja --
https://community.7daystodie.com/topic/34153-littleredsonja-fancy-home-deco-mod-a21/

Installation Instructions:

Extract the downloaded zip into your 7D2D Mods folder (create one if it does not exist).

To verify you have installed the mod correctly, the ModInfo.xml file should be located at Mods\LittleRedSonja Powerable end tables\ModInfo.xml relative to your 7D2D install folder.