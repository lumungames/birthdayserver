War3zuk FarmLife v3 Enhanced Alpha 21.2 B30 Stable

Version 1.24

Adjusted Salt (Sellable)
Adjusted Mustard (Sellable)
Adjusted Black Pepper (Sellable)
Adjusted Salt Shaker (Sellable)

Version 1.23

Fixed Groups (Tables/Grills)
Rebuilt Farm Barrel Model (Unity 2021.3.19F1) Lockable
Added Farm Barrel (Downgrade Model) Unlocked / Damaged
Adjusted FarmLife Magazine (Loot)
Fixed Cooked Braised Fillet Icon

Version 1.22

Fixed Biome Tree Spawning (Oops)
Fixed Brew Cooker (Localization Name)

Removed Old Farmlife.Unity3d File
Removed Old FarmlifeOtherblocks.Unity3d
Removed Old FarmlifeStations.Unity3d
Removed Old ButterChurn.Unity3d
Removed Old FarmTable.Unity3d
Removed Old Oven.Unity3d

Added Farm Forge HD Model (Unity 2021.3.19F1)
Rebuilt Brew Cooker HD Model (Unity 2021.3.19F1)
Rebuilt Wood Grill Model (Unity 2021.3.19F1)
Rebuilt Brewer Station Model (Unity 2021.3.19F1)
Rebuilt Chopping Board Model (Unity 2021.3.19F1)
Rebuilt Kitchen Aid Model (Unity 2021.3.19F1)
Rebuilt Gas Stove Model (Unity 2021.3.19F1)
Rebuilt Scotch Glass Model (Unity 2021.3.19F1)
Rebuilt Webber Grill Model (Unity 2021.3.19F1)
Rebuilt Butter Churn Model (Unity 2021.3.19F1)
Rebuilt / Resized Fermentor Bench Model (Unity 2021.3.19F1)
Rebuilt / Resized Farm Table Model (Unity 2021.3.19F1)

Rebuilt Brick Planter Model (Unity 2021.3.19F1)
Rebuilt Brick Planter 2 Model (Unity 2021.3.19F1)
Rebuilt Brick Planter 3 Model (Unity 2021.3.19F1)
Rebuilt Brick Planter 4 Model (Unity 2021.3.19F1)
Rebuilt Terracotta Planter Model (Unity 2021.3.19F1)
Rebuilt Wood Planter Model (Unity 2021.3.19F1)

Fixed Apple Tree LOD3
Fixed Cherry Tree LOD3
Fixed Pear Tree LOD3

Added Proper Sapling Models (Miniature Of Original Prefab)

Version 1.21

Added Apple Seed (Crafted / Traders)
Added Avocado Seed (Crafted / Traders)
Added Banana Seed (Crafted / Traders)
Added Cherry Seed (Crafted / Traders)
Added Cinnamon Seed (Crafted / Traders)
Added Coconut Seed (Crafted / Traders)
Added Lime Seed (Crafted / Traders)
Added Meyer Lemon Seed (Crafted / Traders)
Added Nut Seed (Crafted / Traders)
Added Olive Seed (Crafted / Traders)
Added Orange Seed (Crafted / Traders)
Added Peach Seed (Crafted / Traders)
Added Pear Seed (Crafted / Traders)
Added Red Apple Seed (Crafted / Traders)
Added Sakura Seed (Crafted / Traders)
Added Sugar Maple Seed (Crafted / Traders)
Added Walnut Seed (Crafted / Traders)
Added White Oak Seed (Crafted / Traders)

Added Apple Tree HD Model (Unity 2021.3.19F1)
Added Avocado Tree HD Model (Unity 2021.3.19F1)
Added Banana Tree HD Model (Unity 2021.3.19F1)
Added Cherry Tree HD Model (Unity 2021.3.19F1)
Added Cinnamon Tree HD Model (Unity 2021.3.19F1)
Added Coconut Tree HD Model (Unity 2021.3.19F1)
Added Lime Tree HD Model (Unity 2021.3.19F1)
Added Meyer Lemon Tree HD Model (Unity 2021.3.19F1)
Added Nut Tree HD Model (Unity 2021.3.19F1)
Added Olive Tree HD Model (Unity 2021.3.19F1)
Added Orange Tree HD Model (Unity 2021.3.19F1)
Added Peach Tree HD Model (Unity 2021.3.19F1)
Added Pear Tree HD Model (Unity 2021.3.19F1)
Added Red Apple Tree HD Model (Unity 2021.3.19F1)
Added Sakura Tree HD Model (Unity 2021.3.19F1)
Added Sugar Maple Tree HD Model (Unity 2021.3.19F1)
Added Walnut Tree HD Model (Unity 2021.3.19F1)
Added White Oak Tree HD Model (Unity 2021.3.19F1)

Removed DrinkJarEmpty (Replaced DrinkCanEmpty)
Added DrinkCanBoiledWater
Adjuted Recipes (Removed Old Jar)

Version 1.20

Fixed Table Unlock

Version 1.19

Added German Local (Sturmsaeufer)
Localization.txt
Blocks.xml
Items.xml
Fixed Blocks
Fixed Trees / Plants
Fixed Icons / Missing
Fixed Apple Tree Code
Fixed Cinnamon Tree Code
Fixed Coconut Tree Code
Fixed Lime Tree Code
Fixed Red Apple Tree Code
Rebuilt Progression Code

Version 1.18

Updated To Alpha 21.1 B16 Stable
Updated Blocks.xml
Update Items.xml
Updated Loot.xml
Fixed Cinnamon Desc Name
Fixed SheepSheers (Thanks Digiox)

Version 1.17

Updated To Alpha 20.7 B1 Stable

Version 1.16

Removed Loot ID34
Edited MarijuanaPlantHarvestPlayer (Hemp)
Fixed Damascus Blade (TAGS)
Fixed Damascus Handle (TAGS)
Fixed Damascus Knife (TAGS)
Removed Special Items (FarmTable) Quests
Fixed Blackberry Seed Unlock

Version 1.15

Edited Cola ItemCode
Edited Chocolate Bar ItemCode
Edited SSM ItemCode
Edited SSM02 ItemCode
Edited SSM03 ItemCode
Edited Hot Chocolate ItemCode
Edited Mashed Potatoes ItemCode
Edited Biscuits and Turkey Gravy ItemCode
Fixed 300 Localization Infos
Adjusted foodEgg Recipe Amount
Added Chicken WorkGroup / XUI
Adjusted Fertile Egg Recipe Amount
Adjusted Loot Slightly (Items)
Adjusted Sugar Cane Recipe

Version 1.14

Fixed 900 Instances Of Broken Desc Info
Fixed Cereal Food/Water Amount
Added Hot Chocolate (Cera)
Added Icon Chicken
Complete Icon Rework By (AlexF221070)
Fixed LootGroup Conflict
Fixed Description Skill Info

Version 1.13

Thanks LucasWayne
Fixed Lime Tree xml
Fixed Apple Tree xml
Fixed Red Apple Tree xml
Fixed Coconut Tree xml
Fixed Cinnamon Tree xml

Added Peach Tree To Biome Spawn
Added Khayotik Custom Changes
Added Dizasta Custom Recipe Changes
Added AlexF221070 SeedPack Icons
Edited Horse XUI
Edited Mature Horse Drop Items
Added Raw Horse Meat
Added Raw Horse Ribs
Added Raw Horse Fillet
Added Cooked Horse Ribs
Added Cooked Braised Fillet
Added Cooked Horse MeatBalls
Added Cooked Horse Ribs Recipe
Added Cooked Braised Fillet Recipe
Added Cooked Horse MeatBalls Recipe
Removed FoodPoisoning (All Items Cooked/Veggies/Berries)
Edited FoodPoisoning (All Raw Meats Now 90% Chance)
Added Cow Windows Code / XUI
Fixed Animal WorkStations Tags For Unlock
Fixed Animal Pens Tags For Unlock
Edited Farm Table Recipe
Removed Duplicate Gas Oven Recipe
Edited Milking Machine (Craft Area Now Farm Table)
Edited bucketEmpty (Craft Area Now Farm Table)
Edited Farm Life Sign (Craft Area Now Farm Table)
Edited Feeding Trough (Craft Area Now Farm Table)
Edited Live Trap (Craft Area Now Farm Table)
Edited Metal Cabinet Door (Craft Area Now Farm Table)
Edited Pinup Girl One (Craft Area Now Farm Table)
Edited Pinup Girl Two (Craft Area Now Farm Table)
Edited Pinup Girl Three (Craft Area Now Farm Table)
Edited Tom Cat (Craft Area Now Farm Table)
Edited Brew Cooker (Craft Area Now Farm Table)
Edited Brew Station (Craft Area Now Farm Table)
Edited Cold Smoker (Craft Area Now Farm Table)
Edited Fermentor Bench (Craft Area Now Farm Table)
Edited Gas Oven (Craft Area Now Farm Table)
Edited KitchNaid (Craft Area Now Farm Table)
Edited Wood Grill (Craft Area Now Farm Table)
Edited Edited Aging Shelves (Craft Area Now Farm Table)
Edited Cheese Press (Craft Area Now Farm Table)
Edited Damascus Handle (Craft Area Now Farm Table)
Edited Damascus Knife (Craft Area Now Farm Table)
Edited Floor Corker (Craft Area Now Farm Table)
Edited Grain Miller (Craft Area Now Farm Table)
Edited Meat Grinder (Craft Area Now Farm Table)
Edited Mixing Cask (Craft Area Now Farm Table)
Edited Oak Barrel (Craft Area Now Farm Table)
Edited Pasteurizer (Craft Area Now Farm Table)
Edited Pizza Cutter (Craft Area Now Farm Table)
Edited Spirits Vat (Craft Area Now Farm Table)
Edited Veggie Attachment (Craft Area Now Farm Table)
Edited Wine Tank (Craft Area Now Farm Table)
Edited Zhang Cleaver (Craft Area Now Farm Table)
Edited Zhang Handle (Craft Area Now Farm Table)
Edited Cheese Cloth (Craft Area Now Farm Table)
Edited Pasta Helper (Craft Area Now Farm Table)

Version 1.12

Removed Class RepairItems Hungry Pens
Edited Triple Taco Tuesday Recipe
Edited Beef Taco Recipe
Fixed Sheep Shears Recipe
Fixed Tortillas Chips Recipes
Edited Fried Rice Item
Edited Pizza Item
Edited Cookie Item
Edited Apples and Cream Recipe
Added Baked Pheasant Recipe
Updated Loot.xml
Replaced All Turkey Icons 256x256
Edited All Craft Times 1s To 10s
Edited All Craft Time 30s To 60s
Added Localization For New Items
Rebuilt Turkey Diner Item
Rebuilt Turkey Diner Stuffed Biscuits Item
Rebuilt Turkey Club Sandwich Item
Adjusted Beef Chili Step 3 Recipe
Adjusted Cooked Kidney Bean Recipe
Adjusted cryogenicContainer Amount To 2 From 1
Added Cherry Tomato Salad with Roasted Lemons Localization
Rebuilt Cherry Tomato Salad with Roasted Lemons Code
Fixed Baked Rigatoni Recipe
Fixed 42 Stack Items
Added Mesquite Woodchips Recipe
Fixed Vegetable Pasta Meal Recipe

Version 1.11

Set All Baby Pens (Grow Anywhere)
Edited Cooked Black Bean Recipe
Edited Cooked Kidney Bean Recipe
Edited Cooked Pinto Bean Recipe
Fixed Fried Rice FoodAmount
Fixed Baked Rigatoni Stacking
Removed AppleWoodPlank (BananaTree)
Fixed Mexican Black Beans Recipe
Removed Un-Used Rolling Mat Recipe
Fixed Spicy Salmon Roll Recipe
Added Raw Lamb Recipe (Raw Mutton)
Fixed 10 Recipes (Damascus Knife)
Added Raw Lamb Mature Sheep
Adjusted Salt Recipes
Added Salt Recipe To KitchnAid
Rebuilt Lamb with Charred Cherry Tomatoes Food
Replaced 1324 Old Lines In Items

Version 1.10

Edited Planters (ResourceWood)
Removed Old Raw Milk Recipe
Added Raw Milk To Adult Holstein
Edited 2 Recipes Removed Buff Replaced
Added Ancho Pepper (Progression)
Adjusted Whole Wheat Recipe
Adjusted Flour Recipe
Adjusted Husk Recipe
Fixed Bagel ToolItem
Fixed Bread ToolItem
Added Ancho Pepper Recipe (GhostPepper Scrap)
Fixed Baby Aninimals Progression
Fixed Baby Chicken Icon
Added Leather To Adult Holstein
Removed cryogenicContainerBee
Removed cryogenicContainerDuck
Removed cryogenicContainerGoat
Removed cryogenicContainerRabbit
Removed LootContainer ID88
Fixed Baby Pig Pen Growing Next
Fixed Duplicate Pizza Recipe
Fixed 32 Recipes
Fixed 172 Tags / Items (Thanks Kraezee)
Removed Halite Recipe (Not Required)

Version 1.09

Fixed Tag Lamb With Charred Tomtatoes
Fixed Tag Penne Pesto
Fixed Tag Turkey Gravy Step 1
Fixed Tag Turkey Gravy Step 2
Fixed Tag Turkey Gravy Step 3
Combined Recipes GasCookTop To Gas Oven
Removed GasCookTop Model
Removed GasCookTop Recipe
Removed GasCookTop Block
Removed GasCookTop Windows
Removed GasCookTop XUI
Added CornHusk To (DeadCorn PlayerCorn)
Removed BeeHive XUI Code
Fixed FarmForge Retrieve Smelted Resources
Fixed Sheep Sheers Tag
Fixed 2 Recipes GasCookTop (Wok)
Fixed 50 Recipes For GasCookTop
Fixed Milk Recipe Area
Removed BeeHive (Un-used Model)
Removed BeeHive HoneyComb (Not Needed)
Removed BeeHive Honey (Not Needed)
Removed BeeHive Recipe (Not Needed)
Fixed 25 Recipes For GasOven
Reset Growth Timers From 2 Testing To (62)

Version 1.08

Removed Old Quests
Removed Old Code Quests/Items
Updated Recipes/Resources Required
Rebuilt Code Hungry Chicken Pen
Rebuilt Code Hungry Pig Pen
Rebuilt Code Hungry Angus Pen
Rebuilt Code Hungry Holstein Pen
Rebuilt Code Hungry Elk Pen
Rebuilt Code Hungry Veal Pen
Rebuilt Code Salmon Pond
Rebuilt Recipe Hungry Chicken Pen
Rebuilt Recipe Hungry Pig Pen
Rebuilt Recipe Hungry Angus Pen
Rebuilt Recipe Hungry Holstein Pen
Rebuilt Recipe Hungry Elk Pen
Rebuilt Recipe Hungry Veal Pen
Rebuilt Recipe Salmon Pond

Version 1.07

Edited KitchnAid (Simplified Addons)
Removed Food Processor Item/Recipe
Removed Ice Cream Bowl Item/Recipe

Edited BrewCooker (Simplified Addons)
Removed Masher Item/Recipe
Removed Barrel Roaster Item/Recipe

Edited Brew Station (Simplified Addons)
Removed Steeping Tun Item/Recipe
Removed Carboy Item/Recipe
Removed Aged Barrell Item/Recipe
Removed Bottle Capper Item/Recipe
Removed Wine Filter Item/Recipe
Removed Charred Barrel Item/Recipe

Edited Fermentor Bench (Simplified Addons)
Removed Jar Airlock Item/Recipe

Removed Old Quests From Removed Addons
Cleaned Old Recipe Code
Cleaned Old Item Code

Version 1.06

Fixed Wood Planks Harvesting
9 Yellow Warnings Removed
Tree Models Replaced / AppleTree
Removed Salt (FridgeLoot)
Fixed 9 Warnings
Removed Tonic Water In Loot

Version 1.05

Removed BasicGrowingTrees Quest
Removed Webber Grill Particle Affect
Fixed Webber Grill Window
Fixed Fermentor Bench Window
Fixed 55 Yellow Warnings
Replaced Saplings With Vanilla Model
Removed FarmLifePlants.Unity3d
Replaced Plants With Vanilla Model
Renamed Saplings
Edited Tree Blocks
Updated Biomes.xml

Version 1.04

Fixed Seed Planted Names
Fixed Seed Packet Names
Fixed Seed Names
Fixed FarmTable Quest Lines
Edited StackAmount 50k
Added FarmLife Skills To Vanilla Tree
Removed Spinning Wheel
Removed BobbinAndFlayer
Removed Bobbin
Removed Flayer
Edited Thread (Craftable FarmTable)
Adjusted Farm Forge TagUnlock (FarmLifeL5)
Adjusted Webber Grill TagUnlock (FarmLifeL5)
Adjusted Rustic Smoker TagUnlock (FarmLifeL5)
Fixed Progression xml
Fixed Tonic Water

Version 1.03

Added War3zuk FarmLife v3 Enhanced VP (Vanilla Patch)
Fixed Particle Clash (War3zuk AIO)
Fixed Fresh Honey Glazed Ham Recipe
Added Cold Smoker Unlock (AdEngineering)
Added Webber Grill Unlock(AdEngineering)
Removed 34 Unused Models
Removed PitchFork
Removed GardenHoe
Removed Bottle Container
Removed Shooting Bottle
Fixed BeefCake / Buff
Removed Butcher Table (Old Model)
Edited HeatMapping All Items

Version 1.02

Added 3rd Slot Farm Forge
Fixed Farm Forge
Fixed Farm Table
Fixed Brew Station Window
Fixed KitchenAid Window
Removed Unused Recipes
Replaced Missing Icons
Fixed Webber Grill Window
Edited FertileLand Number
Rebuilt Trees xml
Rebuilt Sappling xml
Added Biome.xml (Tree spawning in world NOT as a seed for the player)

Version 1.01

Updated Main Modlet Code
Fixed Blocks
Fixed Benches
Fixed Seeds
Fixed Trees
Fixed Most items
Fixed Pens
Fixed Benches
Fixed Buffs
Fixed Windows xml
Fixed UI xml
Fixed Loot